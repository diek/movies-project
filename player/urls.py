from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'movies', views.index, name='home'),
    url(r'^upload/$', views.movie_upload, name='movie_upload'),
]
