from django.db import models


class Movie(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=255, blank=True)
    file_path = models.FileField(upload_to='videos')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
