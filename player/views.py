from django.shortcuts import render, redirect

from .models import Movie
from .forms import MovieForm


def index(request):
    movies = Movie.objects.all()
    return render(request, 'player/index.html', {'movies': movies})


def movie_upload(request):
    if request.method == 'POST':
        form = MovieForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = MovieForm()
    return render(request, 'player/movie_upload.html', {
        'form': form
    })
